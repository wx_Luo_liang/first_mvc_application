﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class StoreController : Controller
    {
        public string Index()
        {
            return "Hello from Store.Index()";
        }

        //public string Browse()
        //{
        //    return "Hello from Store.Browse()";
        //}

        //public string Details()
        //{
            //return "Hello from Store.Details()";
        //}

        //public string Browse(string genre)
        //{
        //    string message = HttpUtility.HtmlEncode("Store.Browse,genre = " + genre);
        //    return message;  
        //}

            public string Browse(string genre)
        {
            string message = HttpUtility.HtmlEncode("Store.Browse,gener = " + genre);
            return message;
        }

        public string Details(int id)
        {
            string message1 = "Store.Details,Id = " + id;
            return message1;
        }
    }
}