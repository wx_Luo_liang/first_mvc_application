﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "你想知道关于我的啥？要啥啥没有，哈哈哈哈！！！";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "联系方式：请拨打XXXXXXXX";

            return View();
        }
    }
}