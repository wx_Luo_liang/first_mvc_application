﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp1.Controllers
{
    public class TestController:Controller
    {
        //控制器的行为
        //ActionResult是个类
        public ActionResult BBC()
        {
            var str = "何当共剪西窗烛";

            return View();
        }

        public string Hello(string ip,string name)
        {
            string res = string.Format("你好，{0},我是你的{1}", ip,name);
            return res;
        }

        public string Hi(int id)
        {
            string res = string.Format("Hi,你的Id={0}", id);
            return res;
        }
    }
}