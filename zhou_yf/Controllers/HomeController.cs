﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public string About()
        {
            var str = "老胡可真厉害呀.";

            ViewBag.Message = str;

            return str;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "我现在告诉你们我的联系方式.";

            return View();
        }
    }
}