﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            var str = "oh my gold";

            return View();
        }
        public string Hello(string ip,string name)
        {
            string str = string.Format("你好,{0},我是{1}", ip,name);

            return str;
        }
        public string Hi(int id)
        {
            string str = string.Format("你的id是{0}",id);

            return str;
        }
    }
}